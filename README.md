# Курс "Введение в Frontend-разработку"

## Инструкции
[Инструкция по заливке ДЗ в Git](Tutorials/homeworks.md)
___
## Домашние заданя
[01. Блог (только HTML)](/Homeworks/01.%20Blog%20(HTML%20only))<br/>
[02. Карточки товаров](/Homeworks/02.%20Shop%20Item)<br/>
[03. Позиционирование элементов](/Homeworks/03.%20Positioning)<br/>
[04. Единицы измерения. Резиновая вёрстка. БЭМ](/Homeworks/03.%20Positioning)<br/>
___
## Конспекты лекций и код
### Введение
[01. Презентация курса](/Lectures/01.%20Intro/)

### HTML и работа с GIT
[02. Понятие страницы сайта, что такое HTML, настройка среды разработки](/Lectures/02.%20HTML%20overview,%20Git,%20Workflow)<br/>
[03. Загрузка ДЗ в Git, HTML теги, структура страницы, атрибуты тегов](/Lectures/03.%20Git%20Homeworks.%20HTML%20Tags,%20DOM)<br/>
[04. Разбиение страницы на блоки, разбиение блоков на элементы](/Lectures/04.%20Page%20structure,%20blocks,%20elements)<br/>
[05. Блочные и строчные элементы, тэги заголовков, тэги div и span, тэг img, тэг a и их атрибуты](/Lectures/05.%20Block%20&%20Inline%20-%20div,%20a,%20span,%20img,%20h1-6)<br/>
[06. Списки, параграфы, таблицы, устаревшие теги. Iframe](/Lectures/06.%20Paragraphs,%20Lists,%20Tables,%20Iframe)<br/>

### CSS
[07. Что такое CSS, способы подключения стилей к странице, селекторы тэга, класса, id](/Lectures/07.%20Intro%20CSS)<br/>
[08. Часто используемые свойства CSS](/Lectures/08.%20Frequently%20used%20CSS%20properies)<br/>
[09. Комбинирование селекторов, приоритет селекторов](/Lectures/09.%20CSS%20Cascade%20Priorities)<br/>
[10. Позиционирование элементов](/Lectures/10.%20Positioning)<br/>
[11. Методология БЭМ](/Lectures/11.%20BEM)<br/>
[12. Единицы измерения размеров. "Резиновая" вёрстка. Свойства min-..., max-…](/Lectures/12.%20Units.%20Rubber%20Layout)<br/>
[13. Разработка форм, тэг input](/Lectures/13.%20Forms)<br/>

